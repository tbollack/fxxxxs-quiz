'use strict';

//Solution service used for communicating with the solution REST endpoints
angular.module('solution').factory('Solution', ['$resource',
  function ($resource) {
    return $resource('api/solution/:solutionId', {
      solutionId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
