'use strict';

// Setting up route
angular.module('solution').config(['$stateProvider',
  function ($stateProvider) {
    // Solution state routing
    $stateProvider
        .state('solution', {
            url: '/solution',
            templateUrl: 'modules/solution/views/solution.client.view.html'
        });
  }
]);
