'use strict';

angular.module('solution').controller('SolutionController', ['$scope', '$stateParams', '$location', 'Authentication', 'Solution', '$http', '$modal',
    function ($scope, $stateParams, $location, Authentication, Solution, $http, $modal) {
        $scope.authentication = Authentication;

        $scope.modalObjects = {};   // modals pass this object full round-trip
        $scope.modalObjects.objImage = {};
        $scope.arrImages = [];      // repeater will loop over this array
        $scope.toJson = angular.toJson;

        $scope.config = {
            strApiKey: '853dcbe812d7c99fcd57c194f6b44c44',
            strTags: 'Soccer',
            intPagerCurrentPage: 1,
            intPagerItemsPerPage: 10,
            getApiUrl: function(){
                return 'https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=' + $scope.config.strApiKey + '&tags=' + encodeURIComponent($scope.config.strTags);
            },
            getPagerTotalItems: function(){
                return $scope.arrImages.length;
            },
            onPagerChange: function(){
                console.log('onPagerChange');
            },
            setPagerItemsPerPage: function(){
                console.log('setPagerItemsPerPage');
                $scope.config.intPagerCurrentPage = 1;
            }
        };

        $scope.parseThumbUrl = function(objImage){
            try{
                return 'https://farm' + objImage.farm + '.staticflickr.com/' + objImage.server + '/' + objImage.id + '_' + objImage.secret + '_s.jpg';
            }catch(err){
                console.log('try catch parseImageUrl err', err);
                return '';
            }
        };

        $scope.parseImageUrl = function(objImage){
            try{
                return 'https://farm' + objImage.farm + '.staticflickr.com/' + objImage.server + '/' + objImage.id + '_' + objImage.secret + '_b.jpg';
            }catch(err){
                console.log('try catch parseImageUrl err', err);
                return '';
            }
        };

        $scope.showImageModal = function(objImage){
            $scope.modalObjects.objImage = objImage;

            var modalInstance = $modal.open({
                templateUrl: 'image-modal.html',
                controller: 'ImageModalController',
                size: 'lg', // sm|md|lg
                scope: $scope,
                resolve: {
                    modalObjects: function () {
                        return $scope.modalObjects;
                    }
                }
            });
            modalInstance.result.then(function (modalReturn) {
                console.log('modalReturn', modalReturn);

            }, function () {
                console.log('modalCancel');

            });
        };

        $scope.showSettingsModal = function(){
            var modalInstance = $modal.open({
                templateUrl: 'settings-modal.html',
                controller: 'SettingsModalController',
                size: 'md', // sm|md|lg
                scope: $scope,
                resolve: {
                    modalObjects: function () {
                        return $scope.modalObjects;
                    }
                }
            });
            modalInstance.result.then(function (modalReturn) {
                console.log('modalReturn', modalReturn);

            }, function () {
                console.log('modalCancel');

            });
        };

        $scope.showErrorModal = function(){
            var modalInstance = $modal.open({
                templateUrl: 'error-modal.html',
                controller: 'ErrorModalController',
                size: 'md', // sm|md|lg
                scope: $scope,
                resolve: {
                    modalObjects: function () {
                        return $scope.modalObjects;
                    }
                }
            });
            modalInstance.result.then(function (modalReturn) {
                console.log('modalReturn', modalReturn);

            }, function () {
                console.log('modalCancel');

            });
        };

        $scope.initGallery = function(){
            $http.get($scope.config.getApiUrl())
                .success(function(objApiResponse) {
                    console.log('$http.get objApiResponse', objApiResponse);
                    if(objApiResponse.stat.toLowerCase() === 'ok'){
                        $scope.arrImages = objApiResponse.photos.photo;
                        $scope.config.intPagerCurrentPage = 1;

                    } else {
                        console.log('objApiResponse input error', objApiResponse, $scope.config.getApiUrl());
                        $scope.modalObjects.err = objApiResponse;

                        $scope.showErrorModal();
                    }
                })
                .error(function(data) {
                    console.log('objApiResponse read error', data, $scope.config.getApiUrl());
                    $scope.modalObjects.err = {message: 'objApiResponse read error', err: data, calling: $scope.config.getApiUrl()};

                    $scope.showErrorModal();
                })
            ;
        };

    }
]);

/* IMAGE MODAL */
angular.module('solution').controller('ImageModalController', function ($scope, $modalInstance, modalObjects) {
    $scope.modalObjects = modalObjects;

    $scope.ok = function () {
        $modalInstance.close($scope.modalObjects);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});

/* SETTINGS MODAL */
angular.module('solution').controller('SettingsModalController', function ($scope, $modalInstance, modalObjects) {
    $scope.modalObjects = modalObjects;

    $scope.ok = function () {
        $scope.initGallery();

        $modalInstance.close($scope.modalObjects);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});

/* ERROR MODAL */
angular.module('solution').controller('ErrorModalController', function ($scope, $modalInstance, modalObjects) {
    $scope.modalObjects = modalObjects;

    $scope.ok = function () {
        $modalInstance.close($scope.modalObjects);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});
