'use strict';

/**
 *  Solution Module dependencies.
 */
var solutionPolicy = require('../policies/solution.server.policy'),
  solution = require('../controllers/solution.server.controller');

module.exports = function (app) {
  // Solution collection routes
  app.route('/api/solution').all(solutionPolicy.isAllowed)
    .get(solution.read);

};
